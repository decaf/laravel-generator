Laravel generator plugin
========================

This is a package for generating files from stubs.

##Installation

###With composer

Add the following lines into your `composer.json` file:

for the repository

	"repositories": [
		{
			"type": "vcs",
			"url": "https://bitbucket.org/decaf/laravel-generator.git"
		}
	]

and require

	"require": {
		"decaf/generator": "dev-master"
	},


##Setup

the service provider needs only to be loaded in local/dev environment!
register the service provider in `app/Providers/AppServiceProvider.php`

    if (config('app.env') === 'local') {
        $this->app->register(\Decaf\Generator\GeneratorServiceProvider::class);
    }


##Config/Stubs

### publish

to publish the config (`config/decaf-generator.php`) & stubs  (`resources/stubs`)

	php artisan vendor:publish

fill in your config values within this file.

##Changelog

* 0.0.1 [2017-09-20]:
	* initial version
