<?php

namespace Decaf\Generator;

use Illuminate\Support\ServiceProvider;
use Decaf\Generator\Console\GeneratorCommand;

/**
 * Class GeneratorServiceProvider
 *
 * @package Decaf\Generator
 */
class GeneratorServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../stubs' => resource_path('/stubs'),
        ], 'stubs');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // merge config
        $configPath = __DIR__ . '/config/config.php';
        $this->mergeConfigFrom($configPath, 'decaf-generator');

        $this->app->singleton('command.decaf.generator', function ($app) {
            return new GeneratorCommand();
        });

        $this->commands('command.decaf.generator');
    }
}
