<?php

namespace Decaf\Generator\Console;

use Illuminate\Console\Command;

class GeneratorCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'decaf:generator';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new files';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        parent::handle();

        dump('handle');
    }

}
